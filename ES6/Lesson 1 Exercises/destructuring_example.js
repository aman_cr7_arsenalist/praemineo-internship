// you can extract data from arrays and objects into distinct variables using destructuring.

// Extracting array elements intro distinct varibales before Destructuring
const names = ["Aman", "Manoj", "Qasim"];

const name1 = names[0];
const name2 = names[1];
const name3 = names[2];

console.log("name 1 = ",name1);       // using , in console.log() gives an default space before printing value
console.log("name 2 = "+name2);
console.log("name 3 = "+name3);

// Extracting array elements in distinct varibales afters Destructuring(ES6)

const [n1,n2,n3] = names;            // [] shows here that we want to destructure an array
console.log("name 1 = "+n1);       
console.log("name 2 = "+n2);
console.log("name 3 = "+n3);

// we can also ignore values while destructuring
const[n4,n5] = names;
console.log("name 2 = "+n4);
console.log("name 3 = "+n5);

// Extracting object properties in distinct varibales afters Destructuring(ES6)
// the varibale name(in which we are storing object property) should be same as object properties otherwise it'll give error

const gemstone = 
{
    type: 'quartz',
    color: 'rose',
    carat: 21.29
};
  
const {type, color, carat} = gemstone;   // {} shows that we want to destructure an object 
  
console.log(type, color, carat);

// selecting a specific property from object
const sbi = 
{
    bankName: "sbi",
    headBranch: "RBI",
    interestRate: 8,
    address: "Shahjahanpur",
    showMessage: function()
    {
        console.log("Welcome to sbi");
    }
};

const{bankName, address,showMessage} = sbi;
console.log(bankName,address);  
showMessage();

