// We can use for..in Loop with iterable objects
// but can't use with Objects because It'not iterable by default

let arr = [1,2,3,4,5];

for(let index in arr)
{
    console.log(arr[index]);
}

// but we shouldn't use for..in Loop with Arrays because 

Array.prototype.decimalfy = function() 
{
    for(let i = 0; i < this.length; i++) 
    {
      this[i] = this[i].toFixed(2);
    }
};

let newArray = [1.222, 1, 4, 5.3483, 6];
  
for(let index in newArray) 
{
    console.log(newArray[index]);
}

// Now for..of Loop can be used with any iterable objects and It should be used with Array compared to for..in Loop.
const digits = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

for (const digit of digits) 
{
  console.log(digit);
}