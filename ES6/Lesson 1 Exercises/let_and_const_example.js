function incrementNumber(n)
{
    if(n % 2 === 0)
    {
        var incrementValue = 2;
        console.log(n + incrementValue);
    }
    else
    {
        // It'll print 'undefined' because before any JavaScript code is executed, all variables declarations are "hoisted", 
        // which means they're raised to the top of the function scope.

        console.log( incrementValue );
    }
}

incrementNumber(3);  

// Above Example should have given an error 'incrementValue not defined'
// but due to hoisting it printed 'undefined'
// To solve this issue, we can either use 'let' or 'const' which have block scope.

function getClothing(isCold) 
{
    if (isCold) 
    {
      let freezing = 'Grab a jacket!';
    } 
    else 
    {
      let hot = 'It’s a shorts kind of day.';
      // Now this console.log(freezing) will give an error if we pass false in getClothing() because freezing isn't defined in this else block.
      console.log(freezing);
    }
}

getClothing(true);

// we can't change the value of const varibales in the scope they have been declared otherwise an error
// and they must have an initial value otherwise an error

const name = "Aman Saxena";

name = "Manoj";   // This statement should give an error that you can't reassing a value to this const variable