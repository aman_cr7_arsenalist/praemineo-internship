// Before applying shorthand property
let type = 'quartz';
let color = 'rose';
let carat = 21.29;

let gemstone1 = 
{
  type: type,
  color: color,
  carat: carat,
  showMessage: function()
  {
      console.log("welcome...");
  }
};

console.log(gemstone1);

// after applying shorthand property (use when object property name and variable being assigned to them has same name)
// we can also write showMessage: function() as showMessage() 
let gemstone2 = { type, color, carat };
console.log(gemstone2);
console.log(Object.keys(gemstone2));
