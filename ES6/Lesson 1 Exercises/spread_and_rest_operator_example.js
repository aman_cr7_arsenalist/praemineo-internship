// spread operator(...) gives you the ability to expand, or spread, iterable objects into multiple individuals elements.

const books = ["Don Quixote", "The Hobbit", "Alice in Wonderland", "Tale of Two Cities"];
console.log(...books);

const fruits = ["apples", "bananas", "pears"];
const vegetables = ["corn", "potatoes", "carrots"];

const produce = [...fruits,...vegetables];
console.log(produce);

// rest parameter (...) combines elements together in one varibale

const order = [20.17, 18.67, 1.50, "cheese", "eggs", "milk", "bread"];
const [total, subtotal, tax, ...items] = order;
console.log(total, subtotal, tax, items);
console.log(typeof items);           // items = array object

// we can use rest paramtere to work with variadic functions (functions which receive indefinite no of arguments)

function sum(...numbers)
{
    let sum = 0;
    for(let number of numbers)
    {
        sum += number;
    }
    return sum;
}

let sumOfTwoNumbers = sum(6,4);
let sumOfThreeNumbers = sum(6,4,5);
console.log(sumOfTwoNumbers);
console.log(sumOfThreeNumbers);

// We could also use 'arguments' object to work with variadic functions 
// which is an array-like object that is available as a local variable inside all functions.
// But it will be confusing because functions definition would look like 
// sum() 
// so programmers might think that this doesn't accept any argument.

function addNumber() 
{
    let total = 0;  
    for(const argument of arguments) 
    {
      total += argument;
    }
    return total;
}

let result1 = addNumber(1,2);
let result2 = addNumber(1,2,3);
console.log(result1);
console.log(result2);

