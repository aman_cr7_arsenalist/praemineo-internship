// we can write expressions inside Template Literals ( OR Template Strings ) which can be evaluated and can work as a placeholder

const name = "Aman Saxena";
console.log(`Welcome, ${name}`);

// Template Literals also preserves newline inside a string.

let quote = `Work hard,
Play harder`;
console.log(quote);

// But if we want to do this using double or single quotes, we'l have to use \n and concatenation operator(+).

let sameQuoteAsAbove = "Work hard\n" +
"Play harder";
console.log(sameQuoteAsAbove);
