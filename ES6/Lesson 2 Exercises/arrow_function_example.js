// Regular Function Example

let names1 = ['Farrin', 'Kagure', 'Asser'];

const upperizedNames1 = names1.map(function(name) 
{ 
    return name.toUpperCase();
}
);
console.log(upperizedNames1);

// Arrow Function Example
// Arrow Function are not used for function declaration
let names2 = ['Aman', 'Manoj', 'Qasim'];

const upperizedNames2 = names2.map( name => name.toUpperCase());
console.log(upperizedNames2);

// Arrow functions are mostly used in three ways:-
// 1st one is :- By storing it in a variable
// 2nd one is :- By passing it as an argument to a function
// 3rd one is :- By using it as object property

let printName = name => `Hello ${name}`;      // Here name is a parameter of arrow function.
console.log(printName("Aman Saxena"));

// But if there are multiple parameters then
let sum = (n1,n2) => n1+n2;
console.log(sum(2,3));

// or there is no parameter
let showMessage = () => "it's an arrow function";  // or let showMessage = _ => "it's an arrow function";
console.log(showMessage());

// Block Body Syntax for an Arrow function

let addNumber = (...numbers) => {
    let sum = 0;
    for(number of numbers)
    {
        sum += number;
    }
    return sum;
};

console.log(addNumber(10,10,10));
console.log(addNumber(5,6));

