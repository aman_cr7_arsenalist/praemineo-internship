// objects before ES6 was created using constructor function.
// now we can use class to create objects but underlying methodology is same as before.

class Student
{
    constructor(name,branch,marks)
    {
        this.name = name;
        this.branch = branch;
        this.marks = marks;
        this.section = "";
    }
    static welcomeMessage()
    {
        console.log( `Welcome to SISTec` );
    }
    findSection()
    {
        if(this.marks > 75)
        {
            this.section = "A";
            return this.section;
        }
        else
        {
            this.section = "B";
            return this.section;
        }
    }
}

Student.welcomeMessage();
let student1 = new Student("Aman Saxena","IT",70);
let student2 = new Student("Manoj Khatarkar","IT",80);
console.log( `Section of ${student1.name} = ${student1.findSection()}` );
console.log( `Section of ${student2.name} = ${student2.findSection()}` );

// Now we can check what is happening under the hood with this class declaration
console.log( `Type of class Student = ${typeof Student}` );           // output = function , it means it still uses normal constructor function to create class