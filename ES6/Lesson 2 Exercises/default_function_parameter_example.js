// without default function parameterS

function greet(name, greeting) 
{
    name = (typeof name !== 'undefined') ?  name : 'Student';
    greeting = (typeof greeting !== 'undefined') ?  greeting : 'Welcome';
  
    return `${greeting} ${name}!`;
}
  
console.log(greet()); // Welcome Student!
console.log(greet('James')); // Welcome James!
console.log(greet('Richard', 'Howdy'));

// with Default function parameter
function greet1(name = 'Student', greeting = 'Welcome') 
{
    return `${greeting} ${name}!`;
}
  
console.log(greet1()); // Welcome Student!
console.log(greet1('James')); // Welcome James!
console.log(greet1('Richard', 'Howdy')); // Howdy Richard!