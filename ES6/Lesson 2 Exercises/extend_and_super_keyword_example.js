class Bank
{
    constructor(headBranch)
    {
        this.headBranch = headBranch;
    }
    calculateRateOfInterest(amt)
    {
        return amt > 10000 ? 10 : 8; 
    }
}

class Sbi extends Bank
{
    constructor(headBranch, name, address)
    {
        // call to super() always be first statement otheriwse it'll give an error
        super(headBranch);
        this.name = name;
        this.address = address;
        this.loanCount = 0;
    }
    loanDisbursedCapacityOfBank(amt)
    {
        if(super.calculateRateOfInterest(amt) >= 10)
        {
            this.loanCount = 20;
            return this.loanCount;
        } 
        else
        {
            this.loanCount = 30;
            return this.loanCount;        
        }
    }
}

let sbi = new Sbi("RBI", "SBI Nawada Indepur", "Distt. Hospital Shahjahanpur");

console.log( `Loan Disturbed Capacity (no of customers it can give loan to) of ${sbi.name} for amount 15000 = ${sbi.loanDisbursedCapacityOfBank(15000)}` );
console.log( `Loan Disturbed Capacity (no of customers it can give loan to) of ${sbi.name} for amount 8000 = ${sbi.loanDisbursedCapacityOfBank(8000)}` );