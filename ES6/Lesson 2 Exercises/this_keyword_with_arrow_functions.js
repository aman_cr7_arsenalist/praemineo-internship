// this keyword in case of arrow functions is evaluated on the basis of "where the function is located and in which context"

function IceCream() 
{
    this.scoops = 0;
}
  
IceCream.prototype.addScoop = function() 
{
    setTimeout(
    () => {                           // an arrow function is passed to setTimeout
      this.scoops++;
      console.log('scoop added!');
    }, 0.5);
};
  
const dessert = new IceCream();
dessert.addScoop();

// run this statement "console.log(dessert.scoops)"" in console after executing this js code
// output will be 1