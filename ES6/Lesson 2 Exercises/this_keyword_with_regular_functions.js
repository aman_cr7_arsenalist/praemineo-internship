// this keyword in case of regular functions is evaluated on the basis of "How the function was callled"

function IceCream() 
{
    this.scoops = 0;
}
  
IceCream.prototype.addScoop = function() 
{
    setTimeout(function() 
    {
      // Here , this is used inside a function which has been called without new, call() or apply() and context object 
      // so here , this will belong to global object , hence scoops will be declared and have value 'undefined' otherwsie 
      this.scoops++;                   // this.scoops should be 1 here.
      console.log('scoop added!');
    }, 500);
};
  
const dessert = new IceCream();
dessert.addScoop();
console.log(dessert.scoops);

// use of closure to resolve the issue in regular functions
/* 
function IceCream2() 
{
    this.scoops = 0;
}
  
IceCream2.prototype.addScoop = function() 
{
    const cone = this;               // sets `this` to the `cone` variable
    setTimeout(function() 
    {
      cone.scoops++;                 // references the `cone` variable
      console.log('scoop added!');
    }, 500);
};
  
const dessert2 = new IceCream();
dessert2.addScoop();
console.log(dessert2.scoops);
*/
