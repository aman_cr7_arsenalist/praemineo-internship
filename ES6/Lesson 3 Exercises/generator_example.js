// There's no way to stop the execution of the function in the middle and pick up again at some later point.
// So in ES6, Generator functions are used to pause the execution of a function in the middle and then continue from where you left off.\

/* 
    Note :- When a generator is invoked, it doesn't actually run any of the code inside the function. Instead, it creates and returns an iterator. 
            This iterator can then be used to execute the actual generator's inner code.
*/

function *getStudent() 
{
    console.log("the function has started");

    const names = ["Aman", "Manoj", "Qasim", "Ravi"];

    for (const name of names) 
    {
        console.log(name);
    }

    console.log('the function has ended');
}

let iteratorObject1 = getStudent();
iteratorObject1.next();                           // This will execute the whole function from start to finish without any pause
console.log("\n")

// Now , to pause the function execution in the middle, we need to use 'yield' (ES6) keyword which can only be used inside generator functions.

function *getStudentInfo() 
{
    console.log("the function has started");

    const names = ["Aman", "Manoj", "Qasim", "Ravi"];

    for (const name of names) 
    {
        console.log(name);
        yield;
    }

    console.log('the function has ended');
}

let iteratorObject2 = getStudentInfo();
iteratorObject2.next();                                // It'll just print first name which is "Aman" , then execution will stop

// if we call next() again, it'll print second name and so on..
iteratorObject2.next();

// We can also send data outside of the function when we pause the function.
function *getBank() 
{
    console.log("the function has started");

    const names = ["SBI", "PNB", "BOB", "OBC"];

    for (const name of names) 
    {
        yield name;
    }

    console.log('the function has ended');
}

let iteratorObject3 = getBank();
let bankName = iteratorObject3.next(); 
console.log(bankName);
console.log(iteratorObject3.next().value);

// We can also send data to the generator function

function* createSundae() 
{
    const toppings = [];

    toppings.push(yield);
    toppings.push(yield);
    toppings.push(yield);

    return toppings;
}

var it = createSundae();

// we can also pass data using next() inside generator when execution is stopped

it.next('hot fudge');                // But this next() will just intitate the generator function , It won't store anything inside toppinngs array
it.next('sprinkles');
it.next('whipped cream');
console.log(it.next());