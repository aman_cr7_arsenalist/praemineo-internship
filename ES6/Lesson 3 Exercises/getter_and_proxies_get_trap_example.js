// With ES5's getter and setter methods, you need to know before hand the properties that are going to be get/set:

let student1 = 
{
    _name: "Aman Saxena",            // _name suggests that it's an private identifier
    _rollNo: 3,
    get name() {
        console.log(`getting the "name" property`);
        console.log(this._name);
    },
    get rollNo() {
        console.log(`getting the "rollNo" property`);
        console.log(this._rollNo);
    }
};

console.log(student1.name);
console.log(student1.rollNo);

student1.city = "Shahjahanpur";            // set a new property on the object
console.log(student1.city);                // logs just Shahjahanpur

// With ES6 Proxies, we do not need to know the properties beforehand:
let student2 = {
    name: "Ravi Mewada",
    rollNo: 12
};

const proxyObj = new Proxy(student2, {
    get(targetObj, property) 
    {
        console.log(`getting the ${property} property`);
        console.log(targetObj[property]);
    }
});

console.log(proxyObj.name);          // logs 'getting the name property' & Ravi Mewada
console.log(proxyObj.rollNo);        // logs 'getting the rollNo property' & 12

proxyObj.city = "Bhopal";            // set a new property on the object
console.log(proxyObj.city);          // logs 'getting the city property' & Bhopal