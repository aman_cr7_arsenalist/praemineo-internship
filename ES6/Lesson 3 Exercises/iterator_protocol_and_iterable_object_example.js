// we have also used for..of Loop in ES6 on iterable objets.
// but for an object to iterable , it must implement the 'iterable' interface means it must contain a default iterator method
// This iterator method defines how that object should be iterated

const digits = [1, 2, 3, 4, 5, 6, 7, 8, 10];
const arrayIterator = digits[Symbol.iterator]();    // This method returns an object which can be used by iterator protocol.

// <object-name>.next() returns a object with two properties :-
// 1) value which represents individual element value in object
// 2) and a boolean value which indicates that if we have reached the end of object elements.
console.log(arrayIterator.next()); 
console.log(arrayIterator.next());
console.log(arrayIterator.next());