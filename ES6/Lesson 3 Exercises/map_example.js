// maps are collection of key-values pairs which means they are similar to objects.
// maps are object where keys and values can either be object or primitive data type.

const employees = new Map();
console.log(employees);               // this will create an empty map

// key-value pairs are added using set()

employees.set("student1", 
{ 
    firstName: "Aman",
    lastName: "Saxena",
    rollNo: 3 
});
employees.set("student2", 
{
    firstName: "Manoj",
    lastName: "Khatarkar",
    rollNo: 6
});
employees.set("student3", 
{
    firstName: "Qasim",
    lastName: "Shakir",
    rollNo: 7
});

console.log(employees);

// Remove a key-value pair from a Map
employees.delete("student1");
console.log(employees);

// Remove all elements from Map
employees.clear();
console.log(employees);

// same like Set, Maps' set() returns map object if successfully added and delete() returns true and false
// Again ,set() and delete() doesn't give an error if it isn't successfully executed.

const members = new Map();

members.set('Evelyn', 75.68);
members.set('Liam', 20.16);
members.set('Sophia', 0);
members.set('Marcus', 10.25);

// To check if a key-value pair exist in a Map
console.log(members.has("Liam"));

// To retrieve a value for a key from Map
console.log(members.get("Marcus"));

// To know how may key-valu pairs are there in a Map
console.log(members.size);

// We can loop over Map in three ways
// 1st one is :-

let iteratorObjForKeys = members.keys();
for(let i=0; i<members.size; i++)
{
    console.log(iteratorObjForKeys.next());
}

// 2nd one is :-
for(let memebr of members)
{
    console.log(memebr);
}

// 3rd one is :-
members.forEach((value, key) => console.log(key, value));