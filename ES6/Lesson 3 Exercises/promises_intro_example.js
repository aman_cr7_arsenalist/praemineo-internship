// A promise will let you start some work that will be done asynchronously and let you get back to your regular work.
// When you create the promise, you must give it the code that will be run asynchronously. 

let returnedObject = new Promise(function(resolve,reject)
{
    window.setTimeout(() => {
        let name = "Aman Saxena";
        console.log("This code line is executed inside Promise.");
        resolve();        // we can pass a argument inside resolve() like name in this case and this argument will be received by then(). 
    },2000);
});

// then() can take two functional arguments 
returnedObject.then(() => console.log("The code inside promise was executed successfully"));

console.log("This line should be executed before Promise.");

// catch() can used to catch if there is any error when we call reject() inside a Promise.