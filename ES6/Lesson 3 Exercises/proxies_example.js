// A proxy object sits between a real object and the calling code. The calling code interacts with the proxy instead of the real object.

let rbi = { status: "Head Branch"};

// The proxy constructor takes two items:
// the object that it will be the proxy for
// an object containing the list of methods it will handle for the proxied object , this object is called handler

let rbiProxy1 = new Proxy(rbi, {});    //  it just passes the request directly to the source object
console.log(rbiProxy1.status);

// If we want the proxy object to actually intercept the request, then we have to use handler object

const richard = {status: 'looking for work'};
const handler = 
{
    get(target, propName) 
    {
        console.log(target); // the `richard` object, not `handler` and not `agent`
        console.log(propName); // the name of the property the proxy (`agent` in this case) is checking
    }
};
const agent1 = new Proxy(richard, handler);

console.log(agent1.status); // logs out the richard object (not the agent object!) and the name of the property being accessed (`status`)

// When console.log(agent.status) is run on the above line, because the get trap exists, 
// it "intercepts" the call to get the status property and runs the get trap function.

// If we want to actually provide the real result, we would need to return the property on the target object:

const manoj = {status: 'looking for work'};
const handler2 = 
{
    get(target, propName) 
    {
        console.log(target);
        console.log(propName);
        return target[propName];     // or we can return whatever we want like a message saying "Keep working hard"
    }
};
const agent2 = new Proxy(manoj, handler2);
console.log(agent2.status);

// So the get trap will take over whenever any property on the proxy is accessed.
// So If we want to intercept calls to change properties, then the set trap needs to be used

const ravi = {status: 'looking for work'};
const handler3 = {
    set(target, propName, value) 
    {
        if (propName === 'payRate')              // if the pay is being set, take 15% as commission
        { 
            value = value * 0.80;
        }
        target[propName] = value;
    }
};
const agent3 = new Proxy(richard, handler3);
agent3.payRate = 2000;                         // set the actor's pay to $1,000
console.log(agent3.payRate);                   // $850 the actor's actual pay

// There are total 13 traps which we can use but we mostly need get() and set() trap


