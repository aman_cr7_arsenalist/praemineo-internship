// Set is a new built-in object (iterable object) that behaves like a mathematical set which means it can't have duplicate values
// Sets are similar to arrays.

let set1 = new Set();        // This will create an empty set
console.log(set1);

let set2 = new Set(["Aman","Manoj","Ravi","Qasim"]);  // This will create a Set which'll have values {1,2,3,4}
console.log(set2);

console.log(set2[1]);      // undefined because set can't be accessed based on index

// if we try to create set with duplicate values

let set3 = new Set(["Aman","Manoj","Ravi","Qasim","Aman"]);          // Here duplicate value will automatically be removed from set.
console.log(set3);

// you can add and delete items from set
set3.add("Faisal");
set3.add("Manoj");    // Here we are adding duplicate item , so It won't be added to the set and this also won't give any error.
console.log(set3);

set3.delete("Qasim");
console.log(set3);

// To remove all elements from set
set2.clear();
console.log(set2);

// add() returns a set if element is successfully added in set
let returnedSet = set3.add("Sidhant");
console.log(returnedSet);

// delete() returns true or false
let returnedBoolean = set3.delete("Manoj");
console.log(returnedBoolean);

// To check number of items in a set
console.log(set3.size);

// checking if an item exist inside a set
console.log(set3.has("Aman"));

// There are two ways of iterating over a Set object
// 1st is as follow:-
let returnedIteratorObject = set3.values();
for(let i=0; i<set3.size; i++)
{
    console.log(returnedIteratorObject.next());
}

// 2nd method is as follows:-
for(let setValue of set3)
{
    console.log(setValue);
}