// Symbol is a new immutable primitve data type in js which is only used to uniquely identify object properties.
// we can pass an optional argument inside Symbol(String argument)
// which describes an Symbol but it's not a part of Symbol.

const s1 = Symbol("Apple");
console.log(s1);
console.log( `Type of value returned by Symbol() = ${typeof s1}` );

const s2 = Symbol("Apple");
console.log(s1 === s2);      // This'll give print false because s1 and s2 are two diffrent symbols. "Apple" just desribes the symbol.

// Problem without symbol data type - if we add two same objects inside another object , last object will override the object before it.
const bowl1 = 
{
    apple: { color: 'red', weight: 136.078 },
    banana: { color: 'yellow', weight: 183.151 },
    orange: { color: 'orange', weight: 170.097 },
    banana: { color: 'yellow', weight: 176.845 }
};
console.log(bowl1);  
console.log(bowl1.apple.color); 

// so , now if we want to add objects as a property inside another object and we have same objects , 
// There are two way we can add them :
// 1) use a diffrent names like apple1 and apple2
// 2) use Symbol(apple) two times and It'll represnt different objects both the times. 

const bowl2 = 
{
    [Symbol("apple")]: { color: 'red', weight: 136.078 },
    [Symbol("banana")]: { color: 'yellow', weight: 183.15 },
    [Symbol("orange")]: { color: 'orange', weight: 170.097 },
    [Symbol("banana")]: { color: 'yellow', weight: 176.845 }
};
console.log(bowl2);

// Now How to access these properties which are defined using Symbol ?