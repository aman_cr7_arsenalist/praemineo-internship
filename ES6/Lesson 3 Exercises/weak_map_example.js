// A Map is weak when it can only contain objects as keys and it's not iterable
// Also a weak map doesn't have clear()

let student1 = { name: "Aman Saxena", rollNo: 3 };
let student2 = { name: "Manoj", rollNo: 6 };
let student3 = { name: "Ravi", rollNo: 12 };

let students = new WeakMap();
students.set(student1, false);
students.set(student2, true);
students.set(student3, true);

console.log(students);

// If we try to add anything other than object in place of key in a weak map, It'll give an error.
// uncomment below statement to find out the error.
// console.log(students.set("aman",false));   

// weak map is useful due to conecpt of Garbage Collection because 
// if we set an key object = null , It's memory will be freed and It no longer can't be used in our program
// so js automatically delete this object reference from weak map. Hence they are very efficient.

student3 = null;
console.log(students);

