// A Set is weak when it can only contain objects and it's not iterable
// Also a weak set doesn't have clear()

let student1 = { name: 'Aman', age: 23, gender: 'male' };
let student2 = { name: 'Ravi', age: 22, gender: 'male' };
let student3 = { name: 'Rinki', age: 21, gender: 'female' };

let students = new WeakSet([student1, student2, student3]);
console.log(students);

// If we try to add anything other than object in weak set, It'll give an error.
// uncomment below statement to find out the error.
// console.log(students.add("Qasim"));   

// weak set is useful due to conecpt of Garbage Collection because 
// if we set an obj = null , It's memory will be freed and It no longer can't be used in our program
// so js automaticall delete this object reference from weak set. Hence they are very efficient.

student3 = null;
console.log(students);

