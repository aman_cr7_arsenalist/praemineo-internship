const inventors = [ {firstName: "Thomas", lastName: "Edison", born: 1847, die: 1931},
                    {firstName: "Nicolas", lastName: "Tesla", born: 1856, die: 1943},
                    {firstName: "Thomas", lastName: "Rutherford", born: 1650, die: 1740},
                    {firstName: "John", lastName: "Beard", born: 1510, die: 1580},
                    {firstName: "Stephen", lastName: "Hawking", born: 1942, die: 2018},
                    {firstName: "Albert", lastName: "Einstein", born: 1879, die: 1955},
                    {firstName: "Isac", lastName: "Newton", born: 1643, die: 1727},
                    {firstName: "Charles", lastName: "Darwin", born: 1809, die: 1882},
                  ];

// Task 1 :- Filter inventors based on the century they were born like 1800s

// filter() calls a provided callback function once for each element in an array, 
// and constructs a new array of all the values for which callback returns a value that coerces to true.
// filter() can take three arguments - the value of the element, the index of the element and the Array object being traversed

const inventorsBornInDecade = inventors.filter( inventor => (inventor.born >= 1800 && inventor.born <=1900) );

console.table(inventorsBornInDecade);         // crates a table view for array elements

// Task 2 :- Return full name of inventors

// map() calls a provided callback function once for each element in an array, in order, and constructs a new array from the results
// and can take three arguments like filter() and map() returns the same no of elements which are in array

const fullNames = inventors.map( inventor => `${inventor.firstName} ${inventor.lastName}` );
console.table(fullNames);

// Task 3 :- Sort inventors by Birthyear in Descending order

// If compareFunction is not supplied, all non-undefined array elements are sorted by converting them to strings and comparing strings in UTF-16 
// code units order. For example, "banana" comes before "cherry". In a numeric sort, 9 comes before 80, but because numbers are converted to 
// strings, "80" comes before "9" in the Unicode order. All undefined elements are sorted to the end of the array.

const sortedInventors = inventors.sort( (lastInventor, nextInventor) => lastInventor.born > nextInventor.born ? -1 : 1 );
console.table(sortedInventors);

// Task 4 :- How many years did all the inventors live

// use reduce() to loop through the array and get a single value as a result

const totalYears = inventors.reduce( (total, inventor) => total + (inventor.die - inventor.born) , 0);
console.log(totalYears);

// Task 5 :- sort inventors according to how much they lived

const oldest = inventors.sort( (lastinventor, nextInventor) =>  (lastinventor.die - lastinventor.born) > (nextInventor.die - nextInventor.born) ? -1 : 1);
console.table(oldest);

const people = ["Saxena, Aman", "Shakir, Qasim", "Khatarkar, Manoj", "Namdev, Sneha", "Mewada, Ravi", "Khan, Faisal", "Ghoshi, Pramod"];

// Task 5 :- Sort people aplhabeticaly according to their last name

const alpha = people.sort( (lastGuy, nextGuy) => {
    let [alast, afirst] = lastGuy.split(", ");
    let [blast, bfirst] = nextGuy.split(", ");
    
    return alast > blast ? 1 : -1;
});

console.table(alpha);

const names = ["aman", "akash", "ravi", "manoj", "akash", "ravi", "rinki", "aman", "manoj", "akash"];

// Task 6:- Calculate instances of each time each individual element occurs in an array

const nameCount = names.reduce( (obj, name) => {
    if(!obj[name])
        obj[name] = 0;
    obj[name]++;
    return obj;
}, {} );

console.log(nameCount);

// Convert a Nodelist into Array -> Array.from(nodeList);
// check if a string contains a substring -> <string-name>.contains("de");