const people = [ {name: "Aman", year: 1997},
                 {name: "Manoj", year: 1996},
                 {name: "Harsh", year: 2018},
                 {name: "Deepak", year: 1982}
               ];

const comments = [ {text: "Love this", id: 523423},
                   {text: "Super good", id: 823423},
                   {text: "You are the best", id: 2039842},
                   {text: "Best of luck", id: 123523},
                   {text: "Nice Nice Nice!", id:542328}
                 ];

// Task 1 :- is at least a person having age 19 ?

const isAdult = people.some( person => {
    const currentYear = (new Date()).getFullYear();
    return currentYear - person.year >= 19;
}); 

console.log(isAdult);

// Task 1 :- is everybody having age 19 ?

const allAdults = people.every( person => {
    const currentYear = (new Date()).getFullYear;
    return currentYear - person.year >= 19;
}); 

console.log(allAdults);

// Task 3 :- Find the comment with id 823423

// find() returns the first element it finds in array which matches the specified condtion in callback function

const comment = comments.find( comment => (comment.id === 823423) );
console.log(comment);

// Task 4 :- Delete comment with ID of 823423

const index = comments.findIndex(comment => (comment.id === 823423) );
console.log(index);

comments.splice(index, 1);     // removing comment with id 823423

console.table(comments);

// or we can also create a new array where we don't store the comment with index 1 (or id=823423)
/*
    const newArray = [ ...slice(0,index), ...slice(index+1) ];
*/