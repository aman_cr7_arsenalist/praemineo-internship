const hero = document.querySelector(".hero");
const heading = document.querySelector("h1");

function shadowEffect(event)
{
    const width = hero.offsetWidth;
    const height = hero.offsetHeight;

    const walk = 100;  // it's in px
    
    let x = event.offsetX;
    let y = event.offsetY;

    if(this !== event.target)
    {
        x = x + event.target.offsetLeft;
        y = y + event.target.offsetTop;
    }

    // we'll move half of the walk either on left side or right side on X axis , so for walk = 100 , we'll move around -50 to 50
    const xWalk = Math.round((x / width * walk) - (walk / 2));     
    const yWalk = Math.round((y / height * walk) - (walk / 2));

    heading.style.textShadow = `${xWalk}px ${yWalk}px 0 rgba(240,0,240,0.5)`;
}

hero.addEventListener("mousemove", shadowEffect);