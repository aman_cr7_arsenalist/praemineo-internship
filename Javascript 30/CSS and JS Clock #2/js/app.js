function setTime()
{
    const presentTime = new Date();

    // logic for second hand of clock
    const sec = presentTime.getSeconds();
    const secDegrees = (sec/60) * 360 + 90;                                           // 90 is added to counter the fact that because initially hands were transformed at 90 degree
    document.querySelector(".sec-hand").style.transform = `rotate(${secDegrees}deg)`;

    // logic for minute hand of clock
    const minutes = presentTime.getMinutes();
    const minutesDegrees = (minutes/60) * 360 + 90;                                                
    document.querySelector(".min-hand").style.transform = `rotate(${minutesDegrees}deg)`;

    // logic for hour hand of clock
    const hours = presentTime.getHours();
    const hoursDegrees = (hours/12) * 360 + 90;
    document.querySelector(".hour-hand").style.transform = `rotate(${hoursDegrees}deg)`;

    document.querySelector("#time").textContent = `${hours} : ${minutes} : ${sec}`;
}

setInterval(setTime, 1000);
