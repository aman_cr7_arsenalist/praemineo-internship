let countdown;
const timerDisplay = document.querySelector(".display__time-left");
const endTime = document.querySelector(".display__end-time");
const buttons = document.querySelectorAll("[data-time]");

function timer(seconds)
{
  // clear existing timer
  clearInterval(countdown);

  const now = Date.now();         // now = ms and you can also use (new Date()).getTime()
  const then = now + seconds * 1000;
  displayTimeLeft(seconds);
  displayEndTime(then);

  countdown = setInterval(() => {
     const secondsLeft = Math.round((then - Date.now()) / 1000);
     if(secondsLeft < 0)
     {
        clearInterval(countdown);
        return;
     }
     displayTimeLeft(secondsLeft);
  }, 1000);
}

function displayTimeLeft(seconds)
{
  const minutes = Math.floor(seconds / 60);
  const remainderSeconds = seconds % 60; 
  const display = `${minutes}:${remainderSeconds < 10 ? "0": ""}${remainderSeconds}`;
  document.title = display;            // To show the timer on the Title of web page
  timerDisplay.textContent = display;
}

function displayEndTime(timestamp)
{
  const end = new Date(timestamp);
  const hour = end.getHours();
  const minutes = end.getMinutes();
  endTime.textContent = `Be back At ${hour > 12 ? hour - 12 : hour}:${minutes < 10 ? "0" : ""}${minutes}`;
}

function startTimer(event)
{
  const seconds = parseInt(this.dataset.time);
  timer(seconds);  
}

buttons.forEach(button => button.addEventListener("click", startTimer));

document.customForm.addEventListener("submit", function(event) {
  event.preventDefault();
  const mins = this.minutes.value;
  timer(mins * 60);
  this.reset();      // if you again submit a time before another one completes, It'll restart the timer for your new time.
});