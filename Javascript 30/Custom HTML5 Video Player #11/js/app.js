// Get our elements
const player = document.querySelector(".player");
const video = player.querySelector(".viewer");
const toggle = player.querySelector(".toggle");
const skipButtons = player.querySelectorAll("[data-skip]");
const ranges = player.querySelectorAll(".player__slider");

toggle.textContent = "⏯️";

// Build out functions

function togglePlay()
{
    if(video.paused)
    {
        video.play();
    }
    else
    {
        video.pause();
    }
}

function updateButton()
{
    const icon = this.paused ? "9654" : "9616";
    toggle.textContent = String.fromCodePoint(icon); 
}

function skip()
{
    console.log(this.dataset.skip);
    video.currentTime = video.currentTime + parseInt(this.dataset.skip);
}

function handleRangeUpdate()
{
    // console(this.name);
    // console.log(this.value);
    video[this.name] = this.value;
}

// Hook up the event listeners

video.addEventListener("click",togglePlay);
video.addEventListener("play",updateButton);
video.addEventListener("pause",updateButton);
toggle.addEventListener("click",togglePlay);

skipButtons.forEach(button => button.addEventListener("click", skip));

ranges.forEach(range => range.addEventListener("change", handleRangeUpdate));

