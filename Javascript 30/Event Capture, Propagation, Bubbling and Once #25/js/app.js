 const divs = document.querySelectorAll("div");
 
 function logText(event)
 {
     console.log(this.classList.value);
     // event.stopPropagation();   // stop bubbling
 }

 divs.forEach(div => div.addEventListener("click", logText, {
     capture: false,
     once: true          // It'll remove or unbind eventListener after it has been called once.
 }));

 // When any event happens , it starts from top (<html>) and goes all the way to the element where event has been triggered.
 // if capture: true , It'll trigger all the events it encounter from top to bottom.
 // if capture: false, It'll trigger all the events from bottom to top(<html>) after the desired event has been triggered.