// const checkbox = document.querySelector("input");
// checkbox.checked = true;
// event.keyCode = 16 (for shift)
// But "keypress" doesn't work for Shift Key.

const checkboxes = document.querySelectorAll(`.inbox input[type="checkbox"]`);
// console.log(checkboxes);

let lastChecked;

function handleCheck(event)
{
    // check if shift key is down and checkbox has been checked
    let inBetween = false;
    if(event.shiftKey && this.checked)
    {
        checkboxes.forEach(checkbox => {
            if(checkbox === this || checkbox === lastChecked)
            {
                inBetween = !inBetween;
            }

            if(inBetween)
            {
                checkbox.checked = true;
            }
        });
    }

    lastChecked  = this;
}

checkboxes.forEach(checkbox => checkbox.addEventListener("click", handleCheck));