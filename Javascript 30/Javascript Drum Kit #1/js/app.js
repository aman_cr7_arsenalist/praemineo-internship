// for playing sound on key press listed on web page

function playSound(event)
{
    const audio = document.querySelector(`audio[data-key = "${event.keyCode}"]`);
    const key = document.querySelector(`div[data-key = "${event.keyCode}"]`);
    
    if(!audio)
        return;
    
    audio.currentTime = 0;          // audio playback start from the begining as soon as you press thhe key as much time as yo want
    audio.play();
    
    key.classList.add("playing");
}

// for removing key press effect from html element(div with class="key")

function removeTransition(event)
{
    if(event.propertyName !== "transform")
        return;
    this.classList.remove("playing");
}

window.addEventListener("keydown", playSound);

const keys = document.querySelectorAll(".key");
keys.forEach(key => key.addEventListener("transitionend", removeTransition));