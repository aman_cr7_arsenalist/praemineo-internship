const images = document.querySelectorAll(".slider-in");

// this function is used to slow down the execution cycle of function passed in scroll event listener. Otherwise It may affect our app performance.
// handleScroll() will be executed after every 20ms we scroll the web page.
// debounce() can also be directly called from including external js library.

function debounce(func, wait = 20, immediate) 
{
	var timeout;
    return function() 
    {
		var context = this, args = arguments;
        var later = function() 
        {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
        if (callNow) 
            func.apply(context, args);
	};
};

function handleScroll(event)
{
    console.count(event);
    images.forEach(image => {
        // Half way through the image
        const slideInAt = (window.scrollY + window.innerHeight) - image.height / 2;

        // bottom of the image
        const imageBottom = image.offsetTop + image.height;

        const isHalfShown = slideInAt > image.offsetTop;

        const isNotScrolledPast = window.scrollY < imageBottom;

        if(isHalfShown && isNotScrolledPast)
        {
            image.classList.add("active");
        }
        else
        {
            image.classList.remove("active");
        }
    });
}

window.addEventListener("scroll", debounce(handleScroll));