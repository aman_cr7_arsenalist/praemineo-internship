const video = document.querySelector(".player");
const canvas = document.querySelector(".photo");
const context = canvas.getContext("2d");
const strip = document.querySelector(".strip");
const snap = document.querySelector(".snap");

function getVideo()
{
    // navigator object contains information about the browser like its name, version, engine, isOnline etc.
    // The Navigator.mediaDevices read-only property returns a MediaDevices object
    // which provides access to connected media input devices like cameras and microphones, as well as screen sharing.

    navigator.mediaDevices.getUserMedia({ video: true, audio: false})
        .then(localMediaStream => {
            console.log(localMediaStream);

            // Now if we an use video.src property which needs a URl , we'll need to convert localMediaStream to A URl using window.URL.createObjectURL().
            // OR we can directly use video.srcObject property and can directly pass localMediaStream to it.

            video.srcObject = localMediaStream;
            video.play();  
        })
        .catch(error => {
            console.error("Oh no..",error);
        });
}

function showVideoInCanvas()
{
    const width = video.videoWidth;
    const height = video.videoHeight;
    canvas.width = width;
    canvas.height = height;

    return setInterval(() => {
        context.drawImage(video, 0, 0, width, height);
        let pixels = context.getImageData(0, 0, width, height);

        // change pixels value in Canvas
        // pixels = redEffect(pixels);           // 1st effect

        pixels = rgbSplit(pixels);               // 2nd effect
        context.globalAplha = 0.8;        
        
        // pixels = greenScreen(pixels);         // 3rd effect

        // after changine pixels values, return them to canvas
        context.putImageData(pixels, 0, 0);
    }, 16);
}

function takePhoto()
{
    snap.currenTime = 0;
    snap.play();

    // take the data out of Canvas
    const data = canvas.toDataURL("image/jpeg");
    const link = document.createElement("a");
    link.href = data;
    link.setAttribute("download","handsome");
    link.innerHTML = `<img src="${data}" alt="Your Pic"/>`;
    strip.insertBefore(link, strip.firstChild);
}

function redEffect(pixels)
{
    for(let i=0; i< pixels.data.length; i+=4)
    {
       pixels.data[i + 0] = pixels.data[i + 0] + 200;   // Red
       pixels.data[i + 0] = pixels.data[i + 0] - 50;   // Green
       pixels.data[i + 0] = pixels.data[i + 0] * 0.5 ;   // Blue
    }
    return pixels;
}

function rgbSplit(pixels)
{
    for(let i=0; i< pixels.data.length; i+=4)
    {
       pixels.data[i - 150] = pixels.data[i + 0];   // Red
       pixels.data[i + 100] = pixels.data[i + 1];   // Green
       pixels.data[i - 150] = pixels.data[i + 2];   // Blue
    }
    return pixels;    
}

function greenScreen(pixels) {
    const levels = {};
  
    document.querySelectorAll('.rgb input').forEach((input) => {
      levels[input.name] = input.value;
    });
  
    for (i = 0; i < pixels.data.length; i = i + 4) 
    {
      red = pixels.data[i + 0];       // red 
      green = pixels.data[i + 1];     // green
      blue = pixels.data[i + 2];      // blue
      alpha = pixels.data[i + 3];     // alpha
  
      if (red >= levels.rmin
        && green >= levels.gmin
        && blue >= levels.bmin
        && red <= levels.rmax
        && green <= levels.gmax
        && blue <= levels.bmax) 
      {
        // take it out!
        pixels.data[i + 3] = 0;       // setting alpha = 0 will make the image transparent in the min and max range
      }
    }
  
    return pixels;
}

getVideo();

video.addEventListener("canplay", showVideoInCanvas); 