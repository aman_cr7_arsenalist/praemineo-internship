const holes = document.querySelectorAll('.hole');
const scoreBoard = document.querySelector('.score');
const moles = document.querySelectorAll('.mole');

let lastHole;
let timeUp = false;
let score = 0;

// function to generate times that mole show up

function randomTime(min, max) 
{
   return Math.round(Math.random() * (max - min) + min);
}

// to select which hole from mole pop up randomly

function randomHole(holes) 
{
   const idx = Math.floor(Math.random() * holes.length);
   const hole = holes[idx];
   if (hole === lastHole) 
   {
      console.log('Ah nah thats the same one bud');
      return randomHole(holes);
   }
   lastHole = hole;
   return hole;
}

// Moles pop up for random time from randomly selected hole

function peep() 
{
   const time = randomTime(200, 1000);
   const hole = randomHole(holes);
   hole.classList.add('up');
   setTimeout(() => {
   hole.classList.remove('up');
   if(!timeUp) 
     peep();
   }, time);
}

function startGame() 
{
   scoreBoard.textContent = 0;
   timeUp = false;
   score = 0;
   peep();
   setTimeout(() => timeUp = true, 10000)
}

function bonk(e) 
{
   if(!e.isTrusted) 
     return; // cheater!
   score++;
   this.parentNode.classList.remove('up');
   scoreBoard.textContent = score;
}

moles.forEach(mole => mole.addEventListener('click', bonk));