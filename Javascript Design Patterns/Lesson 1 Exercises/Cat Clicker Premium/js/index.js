// buttons to display cat images when clicked by user

let button1 = document.getElementById("cat1");
let button2 = document.getElementById("cat2");
let button3 = document.getElementById("cat3");

// varibales to increment no of clicks after every click event on cat image

let count1 = 0;                                     // for image 1
let count2 = 0;                                     // for image 2
let count3 = 0;                                     // for image 3

// event listener for button1 which will show image and no of clicks for cat image 1

button1.addEventListener("click", () => {

    let leftContent = document.createElement("div");
    document.getElementsByTagName("body")[0].appendChild(leftContent);
    leftContent.setAttribute("style", "height: 661px; width: 905px; margin: 0; float: left;");

    let para1 = document.createElement("p");
    para1.innerHTML = "Persian Cat";
    leftContent.appendChild(para1);

    let imageOfCat = document.createElement("img");
    imageOfCat.src = "images/cat.PNG";
    imageOfCat.id = "cat_image1";
    leftContent.appendChild(imageOfCat);
    imageOfCat.setAttribute("style", "height: 600px; width: 805px; cursor: pointer;");

    let rightContent = document.createElement("div");
    document.getElementsByTagName("body")[0].appendChild(rightContent);
    rightContent.setAttribute("style", "float: right; width: 610px; height: 661px; text-align: center;");
    
    let square = document.createElement("div");
    rightContent.appendChild(square);
    square.setAttribute("style", "width: 30%; height: 30%; top: 35%; left: 30%; position: relative; background-color: azure;");

    let result = document.createElement("div");
    square.appendChild(result);
    result.setAttribute("style", "width: 50%; height: 50%; top: 30%; left: 25%; position: relative;");

    let h3 = document.createElement("h3");
    h3.innerHTML = "No of Clicks";
    result.appendChild(h3);

    let para2 = document.createElement("p");
    para2.innerHTML = "0";
    para2.id = "counter1";
    result.appendChild(para2);
    para2.setAttribute("style", "font-size: 20px;");

    // Logic to increment no of clicks on each click event performed by user on cat image 1

    document.getElementById("cat_image1").addEventListener("click", () => {
        document.getElementById("counter1").innerHTML = ++count1;
    });

    // Disable button2 after it has been clicked and image and no of clicks are shown on the web page

    button1.disabled = true;

});

// event listener for button2 which will show image and no of clicks for cat image 2

button2.addEventListener("click", () => {

    let leftContent = document.createElement("div");
    leftContent.setAttribute("style", "height: 661px; width: 905px; margin: 0; float: left;");
    document.getElementsByTagName("body")[0].appendChild(leftContent);

    let para1 = document.createElement("p");
    para1.innerHTML = "Siamese Cat";
    leftContent.appendChild(para1);

    let imageOfCat = document.createElement("img");
    imageOfCat.src = "images/cat2.PNG";
    imageOfCat.id = "cat_image2";
    leftContent.appendChild(imageOfCat);
    imageOfCat.setAttribute("style", "height: 600px; width: 805px; cursor: pointer;");

    let rightContent = document.createElement("div");
    document.getElementsByTagName("body")[0].appendChild(rightContent);
    rightContent.setAttribute("style", "float: right; width: 610px; height: 661px; text-align: center;");
    
    let square = document.createElement("div");
    rightContent.appendChild(square);
    square.setAttribute("style", "width: 30%; height: 30%; top: 35%; left: 30%; position: relative; background-color: azure;");

    let result = document.createElement("div");
    square.appendChild(result);
    result.setAttribute("style", "width: 50%; height: 50%; top: 30%; left: 25%; position: relative;");

    let h3 = document.createElement("h3");
    h3.innerHTML = "No of Clicks";
    result.appendChild(h3);

    let para2 = document.createElement("p");
    para2.innerHTML = "0";
    para2.id = "counter2";
    result.appendChild(para2);
    para2.setAttribute("style", "font-size: 20px;");

    // Logic to increment no of clicks on each click event performed by user on cat image 2

    document.getElementById("cat_image2").addEventListener("click", () => {
        document.getElementById("counter2").innerHTML = ++count2;
    });

    // Disable button2 after it has been clicked and image and no of clicks are shown on the web page

    button2.disabled = true;

});

// event listener for button3 which will show image and no of clicks for cat image 3

button3.addEventListener("click", () => {

    let leftContent = document.createElement("div");
    leftContent.setAttribute("style", "height: 661px; width: 905px; margin: 0; float: left;");
    document.getElementsByTagName("body")[0].appendChild(leftContent);

    let para1 = document.createElement("p");
    para1.innerHTML = "American Bobtail Cat";
    leftContent.appendChild(para1);

    let imageOfCat = document.createElement("img");
    imageOfCat.src = "images/cat3.PNG";
    imageOfCat.id = "cat_image3";
    leftContent.appendChild(imageOfCat);
    imageOfCat.setAttribute("style", "height: 600px; width: 805px; cursor: pointer;");

    let rightContent = document.createElement("div");
    document.getElementsByTagName("body")[0].appendChild(rightContent);
    rightContent.setAttribute("style", "float: right; width: 610px; height: 661px; text-align: center;");
    
    let square = document.createElement("div");
    rightContent.appendChild(square);
    square.setAttribute("style", "width: 30%; height: 30%; top: 35%; left: 30%; position: relative; background-color: azure;");

    let result = document.createElement("div");
    square.appendChild(result);
    result.setAttribute("style", "width: 50%; height: 50%; top: 30%; left: 25%; position: relative;");

    let h3 = document.createElement("h3");
    h3.innerHTML = "No of Clicks";
    result.appendChild(h3);

    let para2 = document.createElement("p");
    para2.innerHTML = "0";
    para2.id = "counter3";
    result.appendChild(para2);
    para2.setAttribute("style", "font-size: 20px;");

    // Logic to increment no of clicks on each click event performed by user on cat image 3

    document.getElementById("cat_image3").addEventListener("click", () => {
        document.getElementById("counter3").innerHTML = ++count3;
    });

    // Disable button3 after it has been clicked and image and no of clicks are shown on the web page
 
    button3.disabled = true;

});