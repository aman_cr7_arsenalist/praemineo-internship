

// Setting name of the first cat image

document.getElementById("name1").innerHTML = "Persian Cat";

// Logic for no of Clicks on first image

let numberOfClicks1 = document.getElementById("cat_image");
let count1 = 0;

numberOfClicks1.addEventListener("click",() => {
    document.getElementById("counter1").innerHTML = ++count1;    
});

// Setting name of the Second cat image

document.getElementById("name2").innerHTML = "Siamens Cat";

// Logic for no of Clicks on Second image

let numberOfClicks2 = document.getElementById("cat_image2");
let count2 = 0;

numberOfClicks2.addEventListener("click",() => {
    document.getElementById("counter2").innerHTML = ++count2;    
});