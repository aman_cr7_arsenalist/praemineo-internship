
let cats = [
    {
        clickCount: 0,
        name: "Tabby",
        imgSrc: "img/22252709_010df3379e_z.jpg",
        nicknames: ["qasi"]
    }
]
let Cat = function() 
{
    this.name = ko.observable("Tabby"); 
    this.clickCount = ko.observable(0);
    this.imgSrc = ko.observable("img/434164568_fea0ad4013_z.jpg");
    this.nicknames = ko.observableArray(["qasi", "momo", "ammu", "bhura"]);

    this.catLevel = ko.computed(function() {
        let clicks = this.clickCount();
        if(clicks <= 10)
        {
            return "Newborn";
        }
        else if(clicks <= 20)
        {
            return "Infant";
        }
        else if(clicks <= 30)
        {
            return "Child";
        }
        else if(clicks <= 40)
        {
            return "Teen";
        }
        else
        {
            return "Adult";
        }
    } , this);
};

let ViewModel = function() {
    let self = this;

    this.catList = ko.observableArray([]);

    cats.forEach( function(catItem) {
        self.catList.push(new Cat(catItem));
    });

    this.currentCat = ko.observable( this.catList()[0] );

    this.incrementCounter = function() 
    {
        self.currentCat().clickCount( self.currentCat().clickCount() + 1 );    
    };    
};

ko.applyBindings(new ViewModel());