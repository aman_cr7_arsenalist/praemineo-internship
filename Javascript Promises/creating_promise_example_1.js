// Promise is a way to perform async task in js
// Promise is a constructor function and we can also store in some variable to perform then and catch later
// Promise is executed inside main() thread

new Promise(function(resolve) 
{
    console.log('first');
    resolve();
    console.log('second');
})
.then(function() 
{
    console.log('third');
});