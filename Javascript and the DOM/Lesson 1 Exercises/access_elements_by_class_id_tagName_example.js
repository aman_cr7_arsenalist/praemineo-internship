// we can access DOM by the document object which is provided by browser.
// document object is not a part of javascript.
// since document is an object, it has some properties and methods which can be used to access and modify the html elments.

let about1 = document.getElementById("about");
console.log(about1);                                             // This prints <p> element with id "about"

// We ca do the same thing by another method.
// The only difference is that we have to access id,class or tag like we do in normal CSS.

let about2 = document.querySelector("#about");
console.log(about2);                                             // same output as console.log(about1)

// To access elements by class attribute, we can again use querySelector() or getElementsByClassname()

let student1 = document.getElementsByClassName("student");       // It'll return all the html elements with class "student".
console.log(student1);                                           // returns a HTML Collection which is an object and has a length property
console.log(typeof student1);                                    

// Accessing individual elements returned by getElementsByClassName()

for(let i=0; i<student1.length; i++)
{
    console.log(student1[i]);
}

let student2 = document.querySelector(".student");               // It'll return only first element with class "student"
console.log(student2);

let student3 = document.querySelectorAll(".student");            // returns a NodeList which is an object and has a length property
console.log(student3);
console.log(typeof student3);

// To access html elements by their tag name
let para1 = document.getElementsByTagName("p");
console.log(para1);

let para2 = document.querySelectorAll("p");
console.log(para2);

// We can apply some conditions in querySelector

console.log(document.querySelector("p.student"));                 // selects <p> element with class "about"

console.log(document.querySelector("p[id]"));                     // selects <p> element with attribute "id"

console.log(document.querySelector("body > h1"));                 // selects <h1> element where parent is <body>