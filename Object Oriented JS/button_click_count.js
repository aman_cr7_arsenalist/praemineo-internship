const button = document.getElementById('button');

// button.js

button.addEventListener('click', 
    (function()                                                     // IIFE
     {
        let count = 0;
    
        return function() {
        count++;
    
        if (count === 2) {
            alert('This alert appears every other press!');
            count = 0;
        }
        };
     }() 
    )
  );

  // Explaination = returned function closes over the count variable. That is, because a function maintains a reference to its parent's scope,
  // count is available for the returned function to use! As a result, we immediately invoke a function that returns that function. And since 
  //the returned function has access to the internal variable, count, a private scope is created -- effectively protecting the data!