// Both call() and apply() invoke a function in the scope of the first argument passed in them

const student1 =
{
    name: "Aman Saxena",
    describe: function () 
    {
      console.log(`${this.name} is a good student`);
    }
};
student1.describe();

// For the first argument of the call() method, we pass in the value to be set as this.
// call() is very effective if you're looking to invoke a function in the scope of the first argument passed into it.
const student2 =
{
    name: "Ravi Mewada"
};
student1.describe.call(student2);

// apply() method differs from call() in syntax only
// apply() takes arguments to a function in a collectio of array and call() takes space seprated arguments.
student1.describe.apply(student2);

// why do we need to save this with anonymous closure

function invokeTwice(cb) {
    cb();
    cb();
 }
 
const dog = 
{
   age: 5,
   growOneYear: function () 
   {
     this.age++;
   }
};

invokeTwice(dog.growOneYear);
console.log(dog.age);                   // This should give output 7 but will give 5 because this is set to global object window here.

// saving this with anonymous closure

invokeTwice(function()                 // This is function calling
{ 
    dog.growOneYear(); 
}
);
console.log(dog.age);

const myGrow = dog.growOneYear.bind(dog);
invokeTwice(myGrow);
console.log(dog.age);

  



