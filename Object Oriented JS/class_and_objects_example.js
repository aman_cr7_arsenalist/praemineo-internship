
function Bank(name)
{
    this.name = name;
}

let sbi = new Bank("State Bank of India");
let bob = new Bank("Bank of Baroda");

console.log(sbi);
console.log(bob);

// how to check object belong to which class or constructor
console.log(sbi instanceof Bank);

// constructor doesn't return anything and nameing convention - always starts from Capital letter
// construction function without parameter
function Student()
{
    this.collegeName = "SISTec";
    this.printMessage = function()
    {
        console.log("Welcome Students of "+this.collegeName);
    };
}
var stud1 = new Student();
console.log(stud1);
console.log(stud1.printMessage());
console.log(stud1 instanceof Student);

// 1)  calling a constructor function with the new keyword sets this to a newly-created object.
// 2)  calling a function that belongs to an object (i.e., a method) sets this to the object itself.
