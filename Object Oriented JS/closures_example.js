// process of a function retaining access to its scope is called a closure. 
// Here, the inner function "closes over" number.
// When a function is declared, it locks onto the scope chain. And
// it will retain this scope chain -- even if it is invoked in a location other than where it was declared. 


function remember(number) {
    return function() {
        return number;
    }
}

const returnedFunction = remember(5); 

console.log( returnedFunction() );

// closure is basically " function + scope of function".
// another simple example of closure

function func1()
{
    var a = 1;
    return function()
    {
        console.log(a);
    }
}

var closure_demo = func1();
closure_demo();