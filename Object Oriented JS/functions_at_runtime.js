// in js , functions are treated as first class functions or (first class objects). Thatswhy they have properties like name and length.
// so we can use function as a value

function sum(a,b)
{
    console.log("Sum Function");
    return a+b;
}

console.log(sum.length);
console.log(sum.name);

// higher order function - a function which returns function
function returnFunction()
{
    console.log("This function will return a function");
    return function() {
        console.log("This is the body of returned function")
    };
}

// There are two ways of using it
var returnedFunction = returnFunction();
returnedFunction();

// OR
returnFunction()();

// forEach() in Array , it takes calback function as an argument which can also be a anonymous function which doesn;t return anything
// callback function inside forEach() takes three arguments - element(required) , index and array (optional)

var arr = [1,2,3,4,5,6,7,8,9,10];
arr.forEach(function(n) {
        if( n % 2 === 0)
            console.log(n);
});

// map() and forEach() have just one difference, map() function returns a new array but it's not compulsory

const names = ['David', 'Richard', 'Veronika'];
const nameLengths = names.map(function(name) {
    return name.length;
  });
console.log("Length of each names in given array="+nameLengths);

// filter() returns array based on a condition
const names1 = ['David', 'Richard', 'Veronika'];
const nameLengths1 = names1.filter(function(name) {
    return name.length < 8;
  });
console.log("Length of each names in given array="+nameLengths1);