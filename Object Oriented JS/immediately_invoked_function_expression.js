// IIFE

// 1st Syntax
(function printName(name)
{
    console.log(name);
}("Aman Saxena"));

// 2nd Syntax
(function printName1(name)
{
    console.log(name);
})("Qasim");