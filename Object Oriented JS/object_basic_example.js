var studentObject1 = {
    name: "Aman Saxena",
    branch: "IT",
    rollNo: 3,
    marks: 79.99
};

// these are the two ways to acess a js object property
console.log(studentObject1["name"]);
console.log(studentObject1.name);

var studentObject2 = {
    "1stname": "Qasim",
    "branch": "IT",
    "rollNo": 7,
    "marks": 75.5
};
console.log(studentObject2["1stname"]);

// console.log(studentObject2.1stname);   // This statement will result in an error. 
// so avaoid using double quotes in key names because you might give incorrect key name according to idenitfier naming rules.

// There are two ways to add property in js object
studentObject1.year = 4;
studentObject1["sem"] = 8;
console.log(studentObject1);

// deleting a property
delete studentObject1.year;
console.log(studentObject1);

// There is one more way to create js object but it slows down the execution of code
var studentObject3 = new Object();
studentObject3.name = "Manoj";
console.log(studentObject3);

studentObject3.showName = function()
{
    return "Welcome,"+this.name;             // Doubt
}
console.log(studentObject3.showName());

// Passing arguments to a function in object
const developer = 
{
    name: "Aman",
    favoriteLanguage: function (language) {
      console.log("My favorite programming language is "+language);  // OR console.log(`My favorite programming language is ${language}`);
    }
};
developer.favoriteLanguage('JavaScript');