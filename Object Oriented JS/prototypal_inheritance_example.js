function Student(name,year) 
{
    this.name = name;
    this.year = year;
}

// The prototype is just an object (or property), and all objects created by a constructor function keep a reference to the prototype
Student.prototype.showInfo = function()
{
    console.log(this.name+" ,year = "+this.year);
}
  
const stud1 = new Student("Aman Saxena",4);
const stud2 = new Student("Manoj",4);
stud1.showInfo();
stud2.showInfo();

// hasOwnProperty() allows you to find the origin of a particular property
function Phone() 
{
    this.operatingSystem = 'Android';
}
  
Phone.prototype.screenSize = 6;

const myPhone = new Phone();
const own = myPhone.hasOwnProperty('operatingSystem');
console.log(own);

const inherited = myPhone.hasOwnProperty('screenSize');
console.log(inherited);

// isPrototypeOf() -  to confirm if an object exists in another object's prototype chain.

Student.prototype = myPhone;                     // This assignment here means , we'll loose showInfo() method which we earlier defined in Student's prototype.
const stud3 = new Student("Qasim",7);
console.log(myPhone.isPrototypeOf(stud3));

// getPrototypeOf()

console.log(Object.getPrototypeOf(stud3));

// constructor property of object
function Longboard() 
{
    this.material = 'bamboo';
}
  
const board = new Longboard();
console.log(board.constructor);  

// Subclass
const Bank = {
    headBranch : "RBI"
}

function Sbi()
{
    this.name = "SBI";
}

Sbi.prototype = Bank;
const branch1 = new Sbi();

// Now we an use properties of child class Sbi also property of Bank (parent object).
console.log(branch1.name);
console.log(branch1.headBranch);

// objects are secretly linked to their constructor's prototype property. the secret link is branch1's __proto__ property
console.log(branch1.__proto__);

/* 
Child.prototype = Parent.prototype  ?
objects are passed by reference. This means that since the Child.prototype object and the Parent.prototype object refer to the same object
-- any changes you make to Child's prototype will also be made to Parent's prototype! We don't want children being able to modify properties 
of their parents!
On top of all this, no prototype chain will be set up.
*/
