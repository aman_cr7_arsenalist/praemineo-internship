const mammal = 
{
    vertebrate: true,
    earBones: 3
};


// rabbit's __proto__ property will point to mammal object  
const rabbit = Object.create(mammal);

console.log(rabbit.__proto__ === mammal);

// now rabit should be able to access mammal's property due to inheritance.
console.log(rabbit.vertebrate);
console.log(rabbit.earBones);