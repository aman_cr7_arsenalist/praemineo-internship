import axios from 'axios';

const instance = axios.create({
    baseURL: "https://burger-builder-88660.firebaseio.com/"
});

export default instance;