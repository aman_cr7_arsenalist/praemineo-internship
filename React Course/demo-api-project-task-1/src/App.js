import React from 'react';
import ContentBuilder from './containers/ContentBuilder/ContentBuilder';

function App() {
  return (
    <div className="App">
      <ContentBuilder />
    </div>
  );
}

export default App;
