import React from 'react';
import classes from './Content.module.css';

const content = (props) => {
    return (
        <div className={classes.Content}>
            <h3>{props.title}</h3>
            <p>{props.body}</p>
            <p style={{color: "red"}}>Button has been disabled, Reload the page again to send HTTP request.</p>
        </div>
    )
};

export default content;