import React, {Component} from 'react';
import classes from './ContentBuilder.module.css';
import Content from '../../components/Content/Content';
import axios from 'axios';

class ContentBuilder extends Component {
    state = {
        sendRequest: false,
        postData: [],
        status: true
    }

    changeSendRequestStatusHandler = () => {
        this.setState({sendRequest: true});
    }

    componentDidUpdate(prevProps) {
        if(this.state.status && this.state.sendRequest !== prevProps.sendRequest)
        {
            axios.get("https://jsonplaceholder.typicode.com/posts")
            .then(response => {
                const newResponse = response.data.slice(0,1);
                this.setState({postData: newResponse, status: false});
            })
            .catch(error => {
                console.log(error);
            });
        }
    }

    render() {
        let post = <p>You haven't send any HTTP request yet.</p>;
        if(this.state.sendRequest) {
            post = this.state.postData.map(post => <Content key={post.id} title={post.title} body={post.body}/>);
        }

        return (
            <div className={classes.ContentBuilder}>
              <button 
                onClick={this.changeSendRequestStatusHandler}
                disabled={this.state.sendRequest}>
                Send HTTP Request
              </button>
              {post}
            </div>
        );
    }
}

export default ContentBuilder;