import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import axios from 'axios';

axios.defaults.baseURL = "https://jsonplaceholder.typicode.com";  // Setting default path which will be added to every get , post or delete request
axios.defaults.headers.common["Authorization"] = "AUTH TOKEN";
axios.defaults.headers.post["Content-Type"] = "application/json";

axios.interceptors.request.use(requestConfig => {
    console.log(requestConfig);
    return requestConfig;         // If we don't return request here , It'll block our request in the component
}, error => {
    console.log(error);
    return Promise.reject(error);
});

const myInterceptor = axios.interceptors.response.use(responseConfig => {
    console.log(responseConfig);
    return responseConfig;
}, error => {
    console.log(error);
    return Promise.reject(error);    // We return the error so that we can also handle the error locally inside a component where error occured.
});

// axios.interceptors.response.eject(myInterceptor);  // Remove interceptor


ReactDOM.render( <App />, document.getElementById( 'root' ) );
registerServiceWorker();
