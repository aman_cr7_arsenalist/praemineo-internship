import React, { Component } from 'react';
import Posts from './Posts/Posts';
import { Route, NavLink, Switch, Redirect } from 'react-router-dom';
import asyncComponent from '../../hoc/asyncComponent';
import './Blog.css';
//import NewPost from './NewPost/NewPost';
const AsyncNewPost = asyncComponent(() => {
  return import('./NewPost/NewPost');
});

class Blog extends Component {
    state = {
        auth: true
    }
    render() {
        return (
            <div className="Blog">
                <header>
                    <nav>
                        <ul>
                            <li><NavLink 
                                to="/posts" 
                                exact
                                activeClassName="active"
                                activeStyle={{
                                    textDecoration: "underline",
                                    color: "#fa923f"
                                }}>Posts</NavLink></li>
                            <li><NavLink to={{
                                pathname: "/new-post",
                                hash: "#submit",
                                search: "?quick-submit=true" 
                            }}>New Post</NavLink></li>
                        </ul>
                    </nav>
                </header>
            {/*<Route path="/" exact render={() => <Posts />}/>*/}
            <Switch>
                { this.state.auth ? <Route path="/new-post" component={AsyncNewPost}/> : null} {/* This scenario no will be handle by Guard, in this case which is <Redirect> */}
                <Route path="/posts" component={Posts}/>
                <Route render={() => <h1>Not Found..</h1>} />
                {/*<Redirect from="/" to="/posts" /> */}     
            </Switch>
            </div>
        );
    }
}

export default Blog;