import React, { useEffect, useRef, useContext } from 'react';     // useEffect is a react Hook which gives functionality of Class based Lifecycle methods for functional components.
import styles from './Cockpit.css';
import AuthContext from '../../context/auth-context';

const cockpit = (props) => {
    const toggleBtnRef = useRef(null);
    const authContext = useContext(AuthContext);

    useEffect(() => {
        console.log("[Cockpit.js] useEffect()");   // this will execute for every render cycle of Cockpit component
        //  HTTP request
        //  setTimeout(() => {
        //    alert("Saved Data to cloud");
        //   } , 1000);
        toggleBtnRef.current.click();
        return () => {
          console.log("[Cockpit.js] clean up work in use effect");  // It'll get executed before main useEffect() when Cockpit is removed but after the first render cycle.
        }
    }, []);

    useEffect(() => {
        console.log("[Cockpit.js] 2nd useEffect()");
        return () => {
          console.log("[Cockpit.js] clean up work in 2nd use effect");  // It'll get executed before main useEffect() for every render cycle.
        }
    });

    const classes = [];
    let btnClass = "";

    if(props.showPersons)
    {
        btnClass = styles.red;
        console.log({btnClass});
    }

    if(props.personsLength <= 2)
      classes.push(styles.red);              // classes = ["red"]
    
    if(props.personsLength <= 1)
      classes.push(styles.bold);             // classes = ["red","blue"]

    return (
        <div className={styles.Cockpit}>
            <h1>{props.title}</h1>
            <h2> Hello, I am Aman Saxena and I have started learning React JS. </h2>
            <p className={classes.join(" ")}> I am from Shahjahanpur, UP. </p>
            <button ref={toggleBtnRef}
                className={btnClass} 
                onClick={props.clicked}>Toggle Persons
            </button>
            <button onClick={authContext.login}>Log in</button>
        </div>
    );
}

export default React.memo(cockpit);