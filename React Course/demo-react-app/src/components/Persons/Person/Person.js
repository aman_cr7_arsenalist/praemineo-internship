import React, { Component } from 'react';
import classes from "./Person.css";
import Aux from "../../../hoc/Auxiliary";
import PropTypes from 'prop-types';
import AuthContext from '../../../context/auth-context';

class Person extends Component {
  constructor(props) {
    super(props);
    this.inputElementRef = React.createRef();  // This is a new approach for refrencing dom elements when they are rendered by react.
  }

  static contextType = AuthContext;   // This property is given by React and is used in place of <AuthContext.Consumer>

  componentDidMount() {
    //this.inputElement.focus();
    this.inputElementRef.current.focus();
    console.log(this.context.autheticated);
  }

  render()
  {
    console.log("[Person.js] rendering...");
    return (
    <Aux classes={classes.Person}>    
      <AuthContext.Consumer>
        {(context) => context.autheticated ? <p>Authenticated</p> : <p>Please Log in</p>}
      </AuthContext.Consumer>   

      { /* OR we can just use , {this.context.authenticated ? <p>Authenticated</p> : <p>Please Log in</p>} 
           and can be used outside your jsx code which wasn't possible before this.
        */ }
            
      <p key="i1" onClick={this.props.click}>I am {this.props.name} and I am {this.props.age} years old.</p>
      <p key="i2">{this.props.children}</p>
      <input 
        key="i3" 
        // ref={(inputEl) => {this.inputElement = inputEl}}  // This is an older approach.
        ref= {this.inputElementRef}
        type="text" 
        onChange={this.props.changed} 
        value={this.props.name}/>
    </Aux>
    );
    }
} 

Person.propTypes = {
  click: PropTypes.func,
  name: PropTypes.string,
  age: PropTypes.number,
  changed: PropTypes.func 
}

export default Person;

// <Aux> is a wrapper component here which is also called Higher Order Component(hoc).
// or we can just pass multiple jsx elements inside an array separated by commas but
// Then we'll also have to give a key attribute to every jsx element inside array.

// we can also use React.Fragment(since React 16.2) instead of <Aux>   