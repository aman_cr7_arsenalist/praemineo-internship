import React, { Component } from 'react';
import Person from './Person/Person';

class Persons extends Component { 
  shouldComponentUpdate(nextProps, nextState) {
    console.log("[Persons.js] shouldComponentUpdate()");
    if(nextProps.persons !== this.props.persons)          // extend PureComponent class instead of Component if yo want to check for every property that has been changed or not, It automatically implements the shouldComponentUpdate() for you
      return true;
    else
      return false;
  }

  getSnapshotBeforeUpdate(prevProps, prevState) {
    console.log("[Persons.js] getSnapshotBeforeUpdate()");
    return {message: "Snapshot.."};
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log("[Persosns.js] ComponentDidUpdate()");
    console.log(snapshot);
  }

  render()
  {
    console.log("[Persons.js] rendering...");
    return this.props.persons.map((person, index) => {
      return (
       <Person click={ () => this.props.clicked(index)}
        name={person.name} 
        age={person.age}
        key={person.id}
        changed={(event) => this.props.changed(event, person.id)}
        // isAuth={this.props.isAuthenticated}
        />
    );
   });
  }
}

export default Persons;