import React, { Component } from 'react';
import styles from './App.css';
import Persons from '../components/Persons/Persons';
import Cockpit from '../components/Cockpit/Cockpit';
import AuthContext from '../context/auth-context';

class App extends Component {
  constructor(props) {                        // Executed when a component is rendered for the first time
    super(props);
    console.log("[App.js] constructor");
  }

  state = {
    persons: [
      { id: "abc1", name: "Rajat", age: 24 },
      { id: "ajs1", name: "Rinki", age: 21 } 
    ],
    showPersons: false,
    showCockpit: true,
    authenticated: false
  }

  static getDerivedStateFromProps(props, state) {                   // after constructor() gets invoked, this is executed.
    console.log("[App.js] getDerivedStateFromProps()",props);
    return state;
  }  

  componentDidMount() {                                             // executed after render()
    console.log("[App.js] ComponentDidMount()");
  }

  /* componentWillMount() {                             // This lifecycle hook isn't used much and there are other lifecycle hookslike this.
    console.log("[App.js] componentWillMount()");
  } 
  */

  shouldComponentUpdate(nextProps, nextState) {         // lifecycle method for state change
    console.log("[App.js] shouldComponentUpdate()");
    return true;              // If I return false here, state change effect won't be shown in our app.
  }

  componentDidUpdate() {                                // Lifecycle method for state change
    console.log("[App.js] ComponentDidUpdate()");
  }

  nameChangedHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    });

    const person = {...this.state.persons[personIndex]};

    person.name = event.target.value;

    const persons = [...this.state.persons];
    persons[personIndex] = person;

    this.setState( {persons: persons} );
  }
 
  togglePersonsHandler = () => {
    const doesShow = this.state.showPersons;
    this.setState({showPersons: !doesShow});
  }

  deletePersonHandler = (personIndex) => {
    const persons = [...this.state.persons];
    persons.splice(personIndex,1);
    this.setState({persons: persons});
  }

  loginHandler = () => {
    this.setState({authenticated: true});
  }

  render() {
    console.log("[App.js] render()");
    let persons = null;

    if(this.state.showPersons)
    {
      persons = <Persons 
              persons = {this.state.persons}
              clicked = {this.deletePersonHandler}
              changed = {this.nameChangedHandler}
              isAuthenticated={this.state.authenticated}/>;
    }

    return (
      // whatever is written inside render() which looks like HTML but It's JSX Syntax. 
      // render() alwyas needs to return a HTML element for the actual DOM to parse it.
      // we can only return one Root Element from render()

        <div className={styles.App}>
          <button 
            onClick={() => this.setState({showCockpit: false})}>
            Remove Button
          </button>
          <AuthContext.Provider 
            value={{autheticated: this.state.authenticated, 
            login: this.loginHandler
            }}>
            {this.state.showCockpit ? (
              <Cockpit
                title={this.props.appTitle} 
                showPersons={this.state.showPersons}
                personsLength={this.state.persons.length}
                clicked={this.togglePersonsHandler}
                login={this.loginHandler}/>
              ) : null}
            {persons}
          </AuthContext.Provider>
        </div>
    );
  // return React.createElement("div",{className: "App"}, React.createElement("h1",null,"Does this work now"));
  }
}

export default App;      // This statement here make sure that if you import this App.js file, you will just importa App class defined here.
